var link = document.querySelector(".header__login");
var popup = document.querySelector(".login-content");
var close = popup.querySelector(".login-content--close");

link.addEventListener("click", function(event){
  event.preventDefault();
  popup.classList.add("login-content--show");
  $("html,body").css("overflow","hidden");
});

close.addEventListener("click", function(event){
  event.preventDefault();
  popup.classList.remove("login-content--show");

$("html,body").css("overflow","auto");
});

var rename = document.querySelector(".profile-info__rename");
var modalRename = document.querySelector(".modal-rename");
var cancel = modalRename.querySelector(".modal-rename__cancel");

rename.addEventListener("click", function(event){
  event.preventDefault();
  modalRename.classList.add("modal-rename__show");
});

cancel.addEventListener("click", function(event){
  event.preventDefault();
  modalRename.classList.remove("modal-rename__show");

});

var city = document.querySelector(".city");
var popupCity = document.querySelector(".popup-city");
var cityCancel = popupCity.querySelector(".cityCancel");

city.addEventListener("click", function(event){
  event.preventDefault();
  popupCity.classList.add("popup-show");
});

cityCancel.addEventListener("click", function(event){
  event.preventDefault();
  popupCity.classList.remove("popup-show");

});

var namber = document.querySelector(".namber");
var popupNamber = document.querySelector(".popup-namber");
var namberCancel = popupNamber.querySelector(".namberCancel");

namber.addEventListener("click", function(event){
  event.preventDefault();
  popupNamber.classList.add("popup-show");
});

namberCancel.addEventListener("click", function(event){
  event.preventDefault();
  popupNamber.classList.remove("popup-show");

});
var site = document.querySelector(".site");
var popupSite = document.querySelector(".popup-site");
var siteCancel = popupSite.querySelector(".siteCancel");

site.addEventListener("click", function(event){
  event.preventDefault();
  popupSite.classList.add("popup-show");
});

siteCancel.addEventListener("click", function(event){
  event.preventDefault();
  popupSite.classList.remove("popup-show");

});
var rename = document.querySelector(".name");
var popupName = document.querySelector(".popup-name");
var nameCancel = popupName.querySelector(".nameCancel");

rename.addEventListener("click", function(event){
  event.preventDefault();
  popupName.classList.add("popup-show");
});

nameCancel.addEventListener("click", function(event){
  event.preventDefault();
  popupName.classList.remove("popup-show");

});

$(document).ready(function(){
    $('.scrollbar-inner').scrollbar();
});
